# It downloads log files from http://api.woogamble.com/logs/ and
# tries to classify its. Downloaded logs placed in folders via
# log file creation date and woogamble version.

try:
    import urllib.request as urllib2
except:
    import urllib2

import re
import sys
import os.path
import shutil
import getopt
import pdb
from datetime import date, timedelta

import error_helper

ERR_MAP = None

def ExtractFromLogfileContext(file_context, myregexp):
    try:
        pattern = re.compile(myregexp)
        res = pattern.search(file_context)
        if res:
            return res.group(1)
    except:
        pdb.set_trace()
        None
    return "undefined"


def GetVersionString2(file_context):
    return ExtractFromLogfileContext(file_context, b'version => ([0-9.]*)\s')


def GetUserName2(file_context):
    return ExtractFromLogfileContext(file_context, b'user => ([0-9a-fA-F-]*)\s')


def GetOsVersion2(file_context):
    return ExtractFromLogfileContext(file_context, b'osVersion => ([0-9.]*)\s')


def GetOVPNserverFromRoute2(file_context):
    #76.164.217.123 -- us.woogamble.com
    #80.84.54.250   -- uk.woogamble.com
    #176.9.148.170  -- de.woogamble.com
    #88.198.65.236  -- de2.woogamble.com
    #178.63.64.20   -- de3.woogamble.com
    openVPNserv = "ovpnserver_undefined"
    ip = re.search(b'routingTable =>[.\s\S]*(76.164.217.123|80.84.54.250|176.9.148.170|88.198.65.236|178.63.64.20)', file_context)
    if ip:
        ipAddr = ip.group(1)
        if ipAddr == b'76.164.217.123':
            openVPNserv = "us"
        elif ipAddr == b'80.84.54.250':
            openVPNserv = "uk"
        elif ipAddr == b'176.9.148.170':
            openVPNserv = "de"
        elif ipAddr == b'88.198.65.236':
            openVPNserv = "de2"
        elif ipAddr == b'178.63.64.20':
            openVPNserv = "de3"

    return openVPNserv


def GetDate(fullpath):
    try:
        myDate = re.search('[0-9]{4}-[0-9]{2}-[0-9]{2}', fullpath)
        if myDate:
            return myDate.group(0)
        print("Undefined date in file name:", fullpath)
    except:
        pdb.set_trace()
    return "undefined"


def FilterLogByVersion33(version):
    try:
        verLastNum = re.search('[0-9]*$', version)
        if verLastNum:
            return int(verLastNum.group(0)) < 33 and int(verLastNum.group(1)) < 1
    except:
        None
    return False


def isCorrectData(date, filterDate):
    if not date or not filterDate:
        return True
    try:
        return date > filterDate
    except:
        pdb.set_trace()
    return False


def LoadFilter(user_file):
    try:
        file = open(user_file, "r")
        user_list = list(file)
        file.close()
        return [user.replace("\n", "") for user in user_list]
    except:
        pdb.set_trace()
        return []

def convertToString(txt):
    if sys.version_info >= (3,0,0):
        # for Python 3
        if isinstance(txt, bytes):
            txt = txt.decode('utf-8')
    else:
        # for Python 2
        if isinstance(txt, unicode):
            txt = str(txt)
    return txt

def copyToSubfolder(filename, list):
    listFolder = []
    for folder in list:
        if None == folder:
            folder = "undefined"
        folder = convertToString(folder)
        listFolder.append(folder)

    path = os.path.join(*listFolder)
    if not os.path.isdir(path):
        os.makedirs(path)

    folder_filename = os.path.join(path, filename)
    shutil.copy( os.path.join('.all', filename), folder_filename)

def getFileContext(filename):
    all_filename = os.path.join('.all', filename)
    file_context = ""
    if os.path.isfile(all_filename):
        log_file = open(all_filename, "rb")
        file_context = log_file.read()
        log_file.close()
    else:
        remotefile = urllib2.urlopen('http://api.woogamble.com/logs/' + filename)
        file_context = remotefile.read()
        remotefile.close()
        # store into 'all' folder
        localfile = open(all_filename, 'wb')
        localfile.write(file_context)
        localfile.close()
    #file_context = str( file_context, encoding='utf-8' )
    return file_context

def foundErrorStr(file_context):
    folderName = []
    if not ERR_MAP:
        return folderName

    for key, errstr in ERR_MAP.items():
        errstr = bytes(errstr, 'utf-8')
        if errstr in file_context:
            folderName.append(key)

    return folderName

def createLogForAnalyze(filename, file_date, file_context, folderNameList):
    file_version = GetVersionString2(file_context)
    # skip log if version earlier than *.*.*.33
    if FilterLogByVersion33(file_version):
        return False

    struct_folder = ["__RESULT__",
                    GetOVPNserverFromRoute2(file_context),
                    file_version,
                    file_date,
                    GetOsVersion2(file_context),
                    GetUserName2(file_context)]

    if folderNameList:
        if len(folderNameList) == 2:
            None
        for folder in folderNameList:
            correct_folder = list(struct_folder)
            correct_folder.insert(2, folder)
            copyToSubfolder(filename, correct_folder)
    else:
        copyToSubfolder(filename, struct_folder)
    return True


def Download(primaryDate, count):
    urlpath = urllib2.urlopen('http://api.woogamble.com/logs/')
    string = urlpath.read().decode('utf-8')
    
    #only for develop
    #file = open("index_of_logs.htm", "r")
    #string = file.read()

    pattern = re.compile('href="(.*.log)"')  # the pattern actually creates duplicates in the list

    tuple = re.findall(pattern, string)
    filelist = set(tuple)

    if not (os.path.isdir('.all')):
        os.mkdir('.all')

    filelist_size = len(filelist)
    filelist_current = 0
    filelist_count = 0
    for filename in filelist:
        try:
            filelist_current += 1
            if count and count <= filelist_count:
                print('max count')
                break

            print(str(filelist_current) + '/' + str(filelist_size), end="\r")

            file_date = GetDate(filename)
            if not isCorrectData(file_date, primaryDate):
                continue

            file_context = getFileContext(filename)

            folderNameList = None
            global ERR_MAP
            if ERR_MAP:
                folderNameList = foundErrorStr(file_context)

            if ERR_MAP and list(folderNameList) == []:
                continue

            if createLogForAnalyze(filename, file_date, file_context, folderNameList):
                filelist_count += 1

        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            err_msg = "Unexpected error:"
            for err_info in sys.exc_info():
                err_msg = err_msg + str(err_info)
            print(err_msg)
            pdb.set_trace()

def choiceOfDates(primaryDate, today):
    result = primaryDate
    if today:
        if primaryDate:
            if primaryDate > today:
                result = today
        else:
            result = today
    return result


def main(argv):
    primaryDate = None
    shiftDate = None
    count = None
    try:
        opts, args = getopt.getopt(argv, "", ["filter-user=", "date=", "shift-date=", "count=", "filter-msg="])
    except getopt.GetoptError:
        print("download_logs.py --filter-user=<filter user file> --date=<date> --count=<count>")
        pdb.set_trace()
        sys.exit(2)
    for opt, arg in opts:
        print( str(opt) + ' = ' + str(arg))
        if opt == "--date":
            primaryDate = arg
        if opt == "--count":
            count = int(arg)
        if opt == "--filter-msg":
            msg_filter = arg
            global ERR_MAP
            ERR_MAP = error_helper.createErrMapDictionary(msg_filter)
        if opt == "--shift-date":
            shift = arg
            shift = int(shift)
            shiftDate=date.today()-timedelta(days=shift)
            print(shiftDate)

    if shiftDate:
        shiftDate = str(shiftDate)
    primaryDate = choiceOfDates(primaryDate, shiftDate)

    Download(primaryDate, count)


if __name__ == "__main__":
    main(sys.argv[1:])

