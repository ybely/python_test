import re
import sys
import pdb

def createErrMapDictionary(user_file):
    try:
        file = open(user_file, "r")
        err_list = list(file)
        file.close()

        D = {}
        for str in err_list:
            pattern = re.compile('\"(.+)\"\s*=>\s*(.+)')
            res = pattern.search(str)
            if res:
                print(res.group(1) + " => " + res.group(2))
                D[res.group(1)] = res.group(2)
        return D

    except:
        err_msg = "error_helper Unexpected error:"
        for err_info in sys.exc_info():
            err_msg = err_msg + str(err_info)
        print(err_msg)
        pdb.set_trace()
        return []